<VirtualHost *:80>
        DocumentRoot "/var/www/uvod_u_php"
        ServerName uvod-u-php.loc
        DirectoryIndex index.php

        <Directory "/var/www/uvod_u_php">
                Options All
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
